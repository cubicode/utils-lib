package co.cubicode.utils.lib;

import java.math.BigDecimal;

import org.junit.Test;

public class NumberToWordsTest {

	@Test
	public void convertionTest() {
		String[] amounts = {
				NumberToWordsUtils.convert(new BigDecimal("144.04")),
				NumberToWordsUtils.convert(new BigDecimal("144.44")),
				NumberToWordsUtils.convert(new BigDecimal("144")),
				NumberToWordsUtils.convert(new BigDecimal("144.00")),
				NumberToWordsUtils.convert(new BigDecimal("14412.00")),
				NumberToWordsUtils.convert(new BigDecimal("444.00")),
				NumberToWordsUtils.convert(new BigDecimal("214.00")),
				NumberToWordsUtils.convert(new BigDecimal("294.00")),
				NumberToWordsUtils.convert(new BigDecimal("103.00")),
				NumberToWordsUtils.convert(new BigDecimal("100.00")),
				NumberToWordsUtils.convert(new BigDecimal("10010.00")),
				NumberToWordsUtils.convert(new BigDecimal("1004.00")),
				NumberToWordsUtils.convert(new BigDecimal("0")),
				NumberToWordsUtils.convert(new BigDecimal("0.00")),
				NumberToWordsUtils.convert(new BigDecimal("0.50")),
				NumberToWordsUtils.convert(new BigDecimal("1")),
				NumberToWordsUtils.convert(new BigDecimal("1.00")),
				NumberToWordsUtils.convert(new BigDecimal("1.5")),
				NumberToWordsUtils.convert(new BigDecimal("1.50")),
				NumberToWordsUtils.convert(new BigDecimal("1.500")),
				NumberToWordsUtils.convert(new BigDecimal("00")),
				NumberToWordsUtils.convert(new BigDecimal("00.00")),
				NumberToWordsUtils.convert(new BigDecimal("101.00")),
				NumberToWordsUtils.convert(new BigDecimal("91.00")),
				NumberToWordsUtils.convert(new BigDecimal("111.00")),
				NumberToWordsUtils.convert(new BigDecimal("71.00")),
				NumberToWordsUtils.convert(new BigDecimal("1001.00")),
				NumberToWordsUtils.convert(new BigDecimal("21.00")),
				NumberToWordsUtils.convert(new BigDecimal("11.00")),
				NumberToWordsUtils.convert(new BigDecimal("31.00")),
				NumberToWordsUtils.convert(new BigDecimal("3100.00")),
				NumberToWordsUtils.convert(new BigDecimal("310001.00")),
				NumberToWordsUtils.convert(new BigDecimal("34234.00")),
				NumberToWordsUtils.convert(new BigDecimal("45000.00")),
				NumberToWordsUtils.convert(new BigDecimal("4500.00")),
				NumberToWordsUtils.convert(new BigDecimal("450.00")),
				NumberToWordsUtils.convert(new BigDecimal("450000.00")),
				NumberToWordsUtils.convert(new BigDecimal("4500000.00")),
		};
		for(String amount : amounts) {
			System.out.println(amount);	
		}
	}
	
}
