package co.cubicode.utils.lib;

import java.util.Calendar;
import java.util.Date;

import junit.framework.Assert;

import org.junit.Test;

public class DateUtilsTest {

	@Test
	public void extendDateToLastSecondOfYearTest() {
		Date date = Calendar.getInstance().getTime();
		Date extendedDate = DateUtils.extendDateToLastSecondOfYear(date);
		Assert.assertNotNull(date);
		System.out.println(extendedDate);
	}

	@Test
	public void truncateDateToFirstSecondOfYearTest() {
		Date date = Calendar.getInstance().getTime();
		Date truncatedDate = DateUtils.truncateDateToFirstSecondOfYear(date);
		Assert.assertNotNull(date);
		System.out.println(truncatedDate);
	}
	
	@Test
	public void extendDateToLastSecondOfMonthTest() {
		Date date = Calendar.getInstance().getTime();
		Date extendedDate = DateUtils.extendDateToLastSecondOfMonth(date);
		Assert.assertNotNull(date);
		System.out.println(extendedDate);
	}

	@Test
	public void truncateDateToFirstSecondOfMonthTest() {
		Date date = Calendar.getInstance().getTime();
		Date truncatedDate = DateUtils.truncateDateToFirstSecondOfMonth(date);
		Assert.assertNotNull(date);
		System.out.println(truncatedDate);
	}
	
}
