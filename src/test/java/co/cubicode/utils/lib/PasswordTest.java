package co.cubicode.utils.lib;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import junit.framework.Assert;

import org.junit.Test;
import org.mindrot.jbcrypt.BCrypt;

public class PasswordTest {

	@Test
	public void hashPasswordTest() throws NoSuchAlgorithmException, InvalidKeySpecException {
		String password = "admin";
		String candidate = "admin";
		String hashedPassword = PasswordUtils.hashPassword(password);
		System.out.println(hashedPassword);
		if (!BCrypt.checkpw(candidate, hashedPassword)) {
			Assert.fail();
		}
	}
}
