package co.cubicode.utils.lib;

import junit.framework.Assert;

import org.junit.Test;

public class RegexUtilsTest {

	@Test
	public void testValidUsernameRegex() {
		String[] usernames = { "asdfas", "aaaaaa" };
		for (String username : usernames) {
			if (!username.matches(RegexUtils.USERNAME_REGEX)) {
				Assert.fail(username + " es un nombre válido");
			}
		}
	}
	
	@Test
	public void testValidPasswordRegex() {
		String[] usernames = { "asdf4DAA", "aaA456aaa" };
		for (String username : usernames) {
			if (!username.matches(RegexUtils.PASSWORD_REGEX)) {
				Assert.fail(username + " es una contraseña válida");
			}
		}
	}

}
