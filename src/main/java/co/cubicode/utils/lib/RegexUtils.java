package co.cubicode.utils.lib;

/**
 * Regex comunes a las aplicaciones
 * 
 * @author juanku
 * 
 */
public class RegexUtils {

	// al menos 8 caracteres, al menos una letra minuscula, al menos una letra mayuscula y al menos un digito
	public static final String PASSWORD_REGEX = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{8,}$";

	// entre 6 y 15 caracteres, puede utilizar letras, digitos y guion bajo
	public static final String USERNAME_REGEX = "^[a-zA-Z0-9_]{6,15}$";

	public static final String EMAIL_REGEX = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

}
