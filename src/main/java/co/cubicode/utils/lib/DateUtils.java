package co.cubicode.utils.lib;

import java.util.Calendar;
import java.util.Date;

/**
 * Clase de utilidades para manejo de fechas
 * 
 * @author juanku
 * 
 */
public class DateUtils {

	private DateUtils() {
		// clase de utilidades, no tiene constructor
	}

	/**
	 * Extiende una fecha al último dia del año
	 * 
	 * @param date
	 *            la fecha a extender
	 * @return la fecha extendida
	 */
	public static Date extendDateToLastSecondOfYear(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.MONTH, 11);
		calendar.set(Calendar.DAY_OF_MONTH, 31);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 999);
		return calendar.getTime();
	}

	/**
	 * Trunca una fecha al primer dia del año
	 * 
	 * @param date
	 *            la fecha a truncar
	 * @return la fecha truncada
	 */
	public static Date truncateDateToFirstSecondOfYear(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.MONTH, 0);
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}

	/**
	 * Extiende una fecha al último dia del mes
	 * 
	 * @param date
	 *            la fecha a extender
	 * @return la fecha extendida
	 */
	public static Date extendDateToLastSecondOfMonth(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 999);
		return calendar.getTime();
	}

	/**
	 * Trunca una fecha al primer dia del mes
	 * 
	 * @param date
	 *            la fecha a truncar
	 * @return la fecha truncada
	 */
	public static Date truncateDateToFirstSecondOfMonth(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}

}
