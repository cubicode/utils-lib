package co.cubicode.utils.lib;

import org.mindrot.jbcrypt.BCrypt;

/**
 * Clase de utilidades para contraseñas
 * 
 * @author juanku
 * 
 */
public class PasswordUtils {

	private PasswordUtils() {
		// clase de utilidades, no tiene constructor
	}

	public static String hashPassword(String password) {
		return hashPassword(password, 12);
	}

	public static String hashPassword(String password, int rounds) {
		return BCrypt.hashpw(password, BCrypt.gensalt(rounds));
	}

	public static Boolean checkPassword(String password, String hashedPassword) {
		return BCrypt.checkpw(password, hashedPassword);
	}

}
